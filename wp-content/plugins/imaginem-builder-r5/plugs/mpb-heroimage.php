<?php
/* Hero Image */
if(!class_exists('em_heroimage')) {
	class em_heroimage extends AQ_Block {

		protected $the_options;
		protected $the_child_options;

		function __construct() {
			$block_options = array(
				'pb_block_icon' => 'simpleicon-star',
				'pb_block_icon_color' => '#E1A43C',
				'name' => __('Hero Image','mthemelocal'),
				'size' => 'span12',
				'tab' => __('Media','mthemelocal'),
				'desc' => __('Add a Hero Image','mthemelocal')
			);
			//create the widget

			$mtheme_shortcodes['heroimage'] = array(
			    'params' => array(),
			    'no_preview' => true,
			    'shortcode_desc' => __('Display Hero image', 'mthemelocal'),
			    'shortcode' => '[heroimage title="{{title}}" subtitle="{{subtitle}}" image="{{image}}" text_location="{{text_location}}" intensity="{{intensity}}" text_decoration="{{text_decoration}}" text="{{text}}" link="{{link}}" icon="{{icon}}" text_slide="{{text_slide}}"]{{content}}[/heroimage]',
			    'popup_title' => __('Generate a Hero image', 'mthemelocal'),
				'params' => array(
			        'image' => array(
			            'std' => '',
			            'type' => 'uploader',
			            'label' => __('Add image', 'mthemelocal'),
			            'desc' => __('Upload an image', 'mthemelocal'),
			        ),
					'intensity' => array(
						'type' => 'select',
						'std' => 'true',
						'label' => __('Intensity for Text and ui elements', 'mthemelocal'),
						'desc' => __('Intensity for Text and ui elements', 'mthemelocal'),
						'options' => array(
							'default' => __('Default','mthemelocal'),
							'bright' => __('Bright','mthemelocal'),
							'dark' => __('Dark','mthemelocal')
						)
					),
					'text' => array(
						'std' => '',
						'type' => 'text',
						'label' => __('Hero image assist text', 'mthemelocal'),
						'desc' => __('Text to display. Displays as a title on bottom of hero image', 'mthemelocal')
					),
					'link' => array(
						'std' => '',
						'type' => 'text',
						'label' => __('Link', 'mthemelocal'),
						'desc' => __('Link for hero image navigation', 'mthemelocal')
					),
					'icon' => array(
						'type' => 'select',
						'std' => 'true',
						'label' => __('Display icon', 'mthemelocal'),
						'desc' => __('Display icon', 'mthemelocal'),
						'options' => array(
							'true' => __('Enable','mthemelocal'),
							'false' => __('Disable','mthemelocal')
						)
					),
					'text_decoration' => array(
						'type' => 'select',
						'std' => 'true',
						'label' => __('Text decoration', 'mthemelocal'),
						'desc' => __('Text decoration', 'mthemelocal'),
						'options' => array(
							'none' => __('None','mthemelocal'),
							'border' => __('Border','mthemelocal'),
							'border-top-bottom' => __('Border Top Bottom','mthemelocal'),
						)
					),
					'text_location' => array(
						'type' => 'select',
						'std' => 'top',
						'label' => __('Text Location', 'mthemelocal'),
						'desc' => __('Text Location', 'mthemelocal'),
						'options' => array(
							'top' => __('Top','mthemelocal'),
							'middle' => __('Middle','mthemelocal'),
							'bottom' => __('Bottom','mthemelocal')
						)
					),
					'text_slide' => array(
						'type' => 'select',
						'std' => 'single',
						'label' => __('Text display.', 'mthemelocal'),
						'desc' => __('Display slideshow of text.', 'mthemelocal'),
						'options' => array(
							'single' => __('Single text','mthemelocal'),
							'slideshow' => __('Slideshow','mthemelocal'),
							'disable' => __('Disable','mthemelocal')
						)
					),
		            'title' => array(
		                'std' => '',
		                'type' => 'text',
		                'label' => __('Title', 'mthemelocal'),
		                'desc' => __('Title', 'mthemelocal'),
		            ),
		            'subtitle' => array(
		                'std' => '',
		                'type' => 'text',
		                'label' => __('Subtitle', 'mthemelocal'),
		                'desc' => __('Subtitle', 'mthemelocal'),
		            ),
		            'content' => array(
		                'std' => __('Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras mattis consectetur purus sit amet fermentum.','mthemelocal'),
		                'type' => 'editor',
		                'label' => __('Service Content', 'mthemelocal'),
		                'desc' => __('Add the service content', 'mthemelocal')
		            )
				)
			);
			$this->the_options = $mtheme_shortcodes['heroimage'];

			parent::__construct('em_heroimage', $block_options);
		}

		function form($instance) {
			$instance = wp_parse_args($instance);

			echo mtheme_generate_builder_form($this->the_options,$instance);
			//extract($instance);
		}

		function block($instance) {
			extract($instance);

			$shortcode = mtheme_dispay_build($this->the_options,$block_id,$instance);

			echo do_shortcode($shortcode);
			
		}
		public function admin_enqueue_scripts(){
			//Any script registers go here
		}
	}
}
