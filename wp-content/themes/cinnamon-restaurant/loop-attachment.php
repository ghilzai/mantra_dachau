<?php
/**
*  loop attachment
 */
?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<?php // get_sidebar(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="entry-wrapper entry-content clearfix">
						<?php
						// Show Image
						if ( wp_attachment_is_image() ) :
							?>
							<div class="attachment-page-image">
							<?php
							echo '<a class="lightbox-active lightbox-image postformat-image-lightbox" data-src="'. wp_get_attachment_url() .'" href="'. wp_get_attachment_url() .'">';
							?>
								<?php
								echo cinnamon_restaurant_display_post_image (
									$post->ID,
									$have_image_url=false,
									$link=false,
									$type="fullwidth",
									$post->post_title,
									$class="" 
								);
								?>
							</a>
							<?php the_excerpt(); ?>
							</div>
							<?php
						endif;
						?>
			
						

		<div class="always-center">
				<?php
				if ( is_single() && !post_password_required() ) {
					get_template_part('/includes/share','this');
				}
				?>
		</div>
						<?php 
						// If comments are open or we have at least one comment, load up the comment template
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
						?>
						<div class="clear"></div>			
					</div>
			</div>

<?php endwhile; // end of the loop. ?>