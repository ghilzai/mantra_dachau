<?php
$reservation_sub_title = cinnamon_restaurant_get_option_data('reservation_sub_title');
$reservation_title = cinnamon_restaurant_get_option_data('reservation_title');
$reservation_description = cinnamon_restaurant_get_option_data('reservation_description');
$reservation_form = cinnamon_restaurant_get_option_data('reservation_form');
$reservation_shortcode = cinnamon_restaurant_get_option_data('reservation_shortcode');

echo '<div class="section-heading none section-align-center" style="padding-top:10px;padding-bottom:10px;margin-bottom:px;">';
echo '<h2 class="entry-sub-title section-sub-title">'.$reservation_sub_title.'</h2>';
echo '<h1 class="entry-title section-title opacity-on">'.$reservation_title.'</h1>';
echo '<div class="section-description"><p>'.$reservation_description.'</p></div>';
echo '</div>';
echo do_shortcode($reservation_shortcode);
?>