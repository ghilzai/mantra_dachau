<?php
/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the "id" fields, make sure to use all lowercase and no spaces.
 *  
 */
function cinnamon_restaurant_options() {
	
	// Pull all Google Fonts using API into an array
	//$fontArray = unserialize($fontsSeraliazed);
	$options_fonts = cinnamon_restaurant_google_fonts();
	
	// Pull all the categories into an array
	$options_categories = array(); 
	array_push($options_categories, "All Categories");
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
    	$options_categories[$category->cat_ID] = $category->cat_name;
	}
	
	// Pull all the pages into an array
	$options_pages = array();
	array_push($options_pages, "Not Selected"); 
	$options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	if ($options_pages_obj) {
		foreach ($options_pages_obj as $page) {
			$options_pages[$page->ID] = $page->post_title;
		}
	}
	
	// Pull all the Featured into an array
	$featured_pages = get_posts('post_type=mtheme_featured&orderby=title&numberposts=-1&order=ASC');
	if ($featured_pages) {
		foreach($featured_pages as $key => $list) {
			$custom = get_post_custom($list->ID);
			if ( isset($custom["fullscreen_type"][0]) ) { 
				$slideshow_type=' ('.$custom["fullscreen_type"][0].')'; 
			} else {
			$slideshow_type="";
			}
			$options_featured[$list->ID] = $list->post_title . $slideshow_type;
		}
	} else {
		$options_featured[0]="Featured pages not found.";
	}
	
	// Pull all the Featured into an array
	$bg_slideshow_pages = cinnamon_restaurant_get_select_target_options('fullscreen_slideshow_posts');
	
	// Pull all the Portfolio into an array
	$portfolio_pages = get_posts('post_type=mtheme_portfolio&orderby=title&numberposts=-1&order=ASC');
	if ($portfolio_pages) {
		foreach($portfolio_pages as $key => $list) {
			$custom = get_post_custom($list->ID);
			$portfolio_list[$list->ID] = $list->post_title;
		}
	}
		
	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/framework/options/images/';
	$theme_imagepath =  get_template_directory_uri() . '/images/';
	$predefined_background_imagepath =  get_template_directory_uri() . '/images/titlebackgrounds/';
		
	$options = array();
		
$options[] = array( "name"			=> esc_html__("General", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-cog',
					"type"			=> "heading");

	$options[] = array( "name" => esc_html__( "Menu type", 'cinnamon-restaurant' ),
						"desc" => esc_html__( "Menu type", 'cinnamon-restaurant' ),
						"id" => "header_menu_type",
						"std" => "left-logo",
						"type" => "images",
						"options" => array(
							'left-logo'	=> $imagepath . 'options/left-logo-menu.png',
							'split-menu' => $imagepath . 'options/split-menu.png',
							'center-logo' => $imagepath . 'options/center-logo-menu.png',
							'vertical-menu'	=> $imagepath . 'options/vertical-menu.png')
						);

	$options[] = array( "name" => esc_html__( "Horizontal Menu Social Header", 'cinnamon-restaurant' ),
						"desc" => esc_html__( "Horizontal Menu social Header", 'cinnamon-restaurant' ),
						"id" => "header_menu_top",
						"std" => "disable",
						"type" => "images",
						"options" => array(
							'disable' => $imagepath . 'options/social-header-no.png',
							'enable' => $imagepath . 'options/social-header.png')
						);

	$options[] = array( "name"			=> esc_html__( "Disable Comments in Pages", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Disables comments in pages even if comments are enabled in page settings.", 'cinnamon-restaurant' ),
						"id"			=> "disable_pagecomments",
						"std"			=> "0",
						"type"			=> "checkbox");

	$options[] = array( "name"			=> esc_html__( "Add Facebook Opengraph Meta tags", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Adds meta tags to recognize featured image , title and decription used in posts and pages.", 'cinnamon-restaurant' ),
						"id"			=> "opengraph_status",
						"std"			=> "0",
						"type"			=> "checkbox");

$options[] = array( "name"			=> esc_html__("Under Construction", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-wrench',
					"type"			=> "heading");

	$options[] = array( "name"			=> esc_html__( "Enable Maintenance Mode", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "<strong>Note:</strong> Maintenance screen is not active for administrators. Admins will need to logout to see maintenance screen.", 'cinnamon-restaurant' ),
						"id"			=> "maintenance_mode",
						"std"			=> "0",
						"type"			=> "checkbox");

	$options[] = array( "name"			=> esc_html__( "Upload Maintenance screeen background.", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Upload Maintenance screeen background.", 'cinnamon-restaurant' ),
						"id"			=> "maintenance_bg",
						"type"			=> "upload");

		$options[] = array( "name"			=> esc_html__( "Text color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Text color", "cinnamon-restaurant" ),
							"id"			=> "maintenance_color",
							"std"			=> "",
							"type"			=> "color");

	$options[] = array( "name" => esc_html__("Under Construction Message", "cinnamon-restaurant" ),
						"desc" => esc_html__("Under construction message.", "cinnamon-restaurant" ),
						"id" => "maintenance_text",
						"std" => "We are under construction and will be back soon.",
						"type" => "textarea");

$options[] = array( "name"			=> esc_html__("API's", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-cogs',
					"type"			=> "heading");

	$options[] = array( "name"			=> esc_html__("Google Map API Key. Required to display Google map","cinnamon-restaurant"),
						"desc"			=> esc_html__("Goole map now requires an API key to display google maps. How to get a ","cinnamon-restaurant") . "<a target='_blank' href='https://developers.google.com/maps/documentation/javascript/get-api-key'>" . esc_html__("Google Map API Key","cinnamon-restaurant") . "</a>",
						"id"			=> "googlemap_apikey",
						"std"			=> "",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__("Instagram Token. Required for Instagram Widget","cinnamon-restaurant"),
						"desc"			=> esc_html__("The Instagram API requires a Token to display user authenticated photos. How to get ","cinnamon-restaurant") . "<a target='_blank' href='http://imaginem.co/instagram-token/'>" . esc_html__("Instagram Token","cinnamon-restaurant") . "</a>",
						"id"			=> "insta_token",
						"std"			=> "",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name" => esc_html__( "Instagram Image limit", "cinnamon-restaurant" ),
						"desc" => esc_html__( "Instagram Image limit", "cinnamon-restaurant" ),
						"id" => "insta_image_limit",
						"std" => "12",
						"min" => "11",
						"max" => "20",
						"step" => "0",
						"unit" => 'images',
						"type" => "text");

	$options[] = array( "name" => esc_html__("Custom CSS", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-cogs',
					"type" => "heading");

	$options[] = array( "name" => esc_html__("Custom CSS", "cinnamon-restaurant" ),
						"desc" => esc_html__("You can include custom CSS to this field.","cinnamon-restaurant") . "<br/>" . esc_html__("example: ","cinnamon-restaurant") . "<br/><code>.entry-title h1 { font-family: 'Lobster', cursive; }</code>",
						"id" => "custom_css",
						"std" => '',
						"class" => "big",
						"type" => "textarea");

	$options[] = array( "name" => esc_html__("Mobile Specific CSS", "cinnamon-restaurant" ),
						"desc" => esc_html__("Only for mobile specific CSS.","cinnamon-restaurant") . "<br/>" . esc_html__("example: ","cinnamon-restaurant") . "<br/><code>.entry-title h1 { font-family: 'Lobster', cursive; }</code>",
						"id" => "mobile_css",
						"std" => '',
						"class" => "big",
						"type" => "textarea");

$options[] = array( "name"			=> esc_html__("Horizontal Menu Logo", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-diamond',
					"type"			=> "heading");

	$options[] = array( "name"			=> esc_html__( "Horizontal Menu Logo", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Logo for bright background.", 'cinnamon-restaurant' ),
						"id"			=> "main_logo",
						"type"			=> "upload");

	$options[] = array( "name"			=> esc_html__( "Horizontal Inverse Menu Logo ( Bright )", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Logo for transparent menu. Menu can be switched to transparent from Page Settings.", 'cinnamon-restaurant' ),
						"id"			=> "main_logo_inverse",
						"type"			=> "upload");

	$options[] = array( "name"			=> esc_html__( "Logo Height", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Logo Height in pixels. Width will be adjusted with height proportion.", 'cinnamon-restaurant' ),
						"id"			=> "logo_height",
						"min"			=> "0",
						"max"			=> "2000",
						"step"			=> "0",
						"unit"			=> 'pixels',
						"std"			=> "50",
						"type"			=> "text");
						
	$options[] = array( "name"			=> esc_html__( "Top Space", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Top spacing for logo ( 0 sets default )", 'cinnamon-restaurant' ),
						"id"			=> "logo_topmargin",
						"min"			=> "0",
						"max"			=> "200",
						"step"			=> "0",
						"unit"			=> 'pixels',
						"std"			=> "26",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__( "Left Space", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Left spacing for logo ( 0 sets default )", 'cinnamon-restaurant' ),
						"id"			=> "logo_leftmargin",
						"min"			=> "0",
						"max"			=> "200",
						"step"			=> "0",
						"unit"			=> 'pixels',
						"std"			=> "0",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__( "Sticky Logo Height", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Sticky Logo Height in pixels", 'cinnamon-restaurant' ),
						"id"			=> "sticky_logo_height",
						"min"			=> "0",
						"max"			=> "2000",
						"step"			=> "0",
						"unit"			=> 'pixels',
						"std"			=> "50",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__( "Sticky Top Space", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Sticky Top spacing for logo ( 0 sets default )", 'cinnamon-restaurant' ),
						"id"			=> "sticky_logo_topmargin",
						"min"			=> "0",
						"max"			=> "200",
						"step"			=> "0",
						"unit"			=> 'pixels',
						"std"			=> "0",
						"type"			=> "text");

$options[] = array( "name"			=> esc_html__("Vertical Menu Logo", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-diamond',
					"type"			=> "heading");

	$options[] = array( "name"			=> esc_html__( "Vertical Menu Logo", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Upload logo for header", 'cinnamon-restaurant' ),
						"id"			=> "vmain_logo",
						"type"			=> "upload");

	$options[] = array( "name"			=> esc_html__( "Vertical Menu Logo Width", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Vertical Menu Logo width in pixels", 'cinnamon-restaurant' ),
						"id"			=> "vlogo_width",
						"min"			=> "0",
						"max"			=> "2000",
						"step"			=> "0",
						"unit"			=> 'pixels',
						"std"			=> "300",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__( "Top Space", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Top spacing for logo ( 0 sets default )", 'cinnamon-restaurant' ),
						"id"			=> "vlogo_topmargin",
						"min"			=> "0",
						"max"			=> "200",
						"step"			=> "0",
						"unit"			=> 'pixels',
						"std"			=> "0",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__( "Left Space", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Left spacing for logo ( 0 sets default )", 'cinnamon-restaurant' ),
						"id"			=> "vlogo_leftmargin",
						"min"			=> "0",
						"max"			=> "200",
						"step"			=> "0",
						"unit"			=> 'pixels',
						"std"			=> "0",
						"type"			=> "text");

$options[] = array( "name"			=> esc_html__("Responsive Logo", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-diamond',
					"type"			=> "heading");

	$options[] = array( "name"			=> esc_html__( "Responsive/Mobile Logo", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Upload logo for responsive layout.", 'cinnamon-restaurant' ),
						"id"			=> "responsive_logo",
						"type"			=> "upload");

	$options[] = array( "name"			=> esc_html__( "Responsive Logo Width", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Responsive Logo width in pixels", 'cinnamon-restaurant' ),
						"id"			=> "responsive_logo_width",
						"min"			=> "0",
						"max"			=> "2000",
						"step"			=> "0",
						"unit"			=> 'pixels',
						"std"			=> "0",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__( "Responsive Logo Top Space", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Top spacing for logo ( 0 sets default )", 'cinnamon-restaurant' ),
						"id"			=> "responsive_logo_topmargin",
						"min"			=> "0",
						"max"			=> "200",
						"step"			=> "0",
						"unit"			=> 'pixels',
						"std"			=> "0",
						"type"			=> "text");

$options[] = array( "name"			=> esc_html__("WP Login Logo", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-diamond',
					"type"			=> "heading");
						
	$options[] = array( "name"			=> esc_html__( "Custom WordPress Login Page Logo", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Upload logo for WordPress Login Page", 'cinnamon-restaurant' ),
						"id"			=> "wplogin_logo",
						"type"			=> "upload");

	$options[] = array( "name"			=> esc_html__( "WP Login logo Width", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Define Logo width in pixels", 'cinnamon-restaurant' ),
						"id"			=> "wplogin_width",
						"min"			=> "0",
						"max"			=> "2000",
						"step"			=> "0",
						"unit"			=> 'pixels',
						"std"			=> "300",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__( "WP Login logo Height", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Define Logo height in pixels", 'cinnamon-restaurant' ),
						"id"			=> "wplogin_height",
						"min"			=> "0",
						"max"			=> "2000",
						"step"			=> "0",
						"unit"			=> 'pixels',
						"std"			=> "300",
						"type"			=> "text");

$options[] = array( "name"			=> esc_html__("Preloader", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-refresh',
					"type"			=> "heading");

	$options[] = array( "name"			=> esc_html__( "Preloader screen Logo ( required )", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Upload logo for preloader screen.", 'cinnamon-restaurant' ),
						"id"			=> "preloader_logo",
						"type"			=> "upload");

	$options[] = array( "name"			=> esc_html__( "Preloader Logo width", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Preloader Logo width in pixels", 'cinnamon-restaurant' ),
						"id"			=> "preloader_logo_width",
						"min"			=> "0",
						"max"			=> "2000",
						"step"			=> "0",
						"unit"			=> 'pixels',
						"std"			=> "0",
						"type"			=> "text");

		$options[] = array( "name"			=> esc_html__( "Preloader animated bar color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Preloader animated bar color", "cinnamon-restaurant" ),
							"id"			=> "preloader_color",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Preloader screen color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Preloader screen color", "cinnamon-restaurant" ),
							"id"			=> "preloader_bgcolor",
							"std"			=> "",
							"type"			=> "color");

$options[] = array( "name"			=> esc_html__("Food Menu", "cinnamon-restaurant" ),
				"icon"			=> 'fa fa-cutlery',
				"type"			=> "heading");

	$options[] = array( "name"			=> esc_html__("Currency","cinnamon-restaurant"),
						"desc"			=> esc_html__("Currency","cinnamon-restaurant"),
						"id"			=> "food_currency",
						"std"			=> "$",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__("Currency Position","cinnamon-restaurant"),
						"desc"			=> esc_html__("Currency Position","cinnamon-restaurant"),
						"id"			=> "food_currency_position",
						"std"			=> "left",
						"type" 			=> "select",
						"class" 		=> "mini",
						"options" 		=> array(
							'left' 		=> "Left",
							'right' 	=> "Right")
						);

	$options[] = array( "name"			=> esc_html__("Label for Chef Recommended","cinnamon-restaurant"),
						"desc"			=> esc_html__("Label for Chef Recommended","cinnamon-restaurant"),
						"id"			=> "food_chef_recommended",
						"std"			=> "Chef Recommended",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__("Label for New Food","cinnamon-restaurant"),
						"desc"			=> esc_html__("Label for New Food","cinnamon-restaurant"),
						"id"			=> "food_new_item",
						"std"			=> "New",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__("Label for Purchasable/Delivery Food","cinnamon-restaurant"),
						"desc"			=> esc_html__("Label for Purchasable/Delivery Food","cinnamon-restaurant"),
						"id"			=> "food_order_item",
						"std"			=> "Order",
						"class"			=> "tiny",
						"type"			=> "text");

	$max_labels = 10;
	$max_labels = cinnamon_get_max_food_labels();
	for ($foodlabel=1; $foodlabel <= $max_labels; $foodlabel++ ) {
						
		$options[] = array( "name"			=> esc_html__("Custom Food Label ", "cinnamon-restaurant" )  . $foodlabel,
						"desc"			=> esc_html__("Activate Labels by naming them. Labels can be selected from food post settings.", "cinnamon-restaurant" ),
						"id"			=> "foodlabel-".$foodlabel,
						"std"			=> "",
						"class"			=> "small",
						"type"			=> "text");
	}

$options[] = array( "name"			=> esc_html__("Reservation", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-cutlery',
					"type"			=> "heading");

	$options[] = array( "name"			=> esc_html__("Reservation sub title","cinnamon-restaurant"),
						"desc"			=> esc_html__("Reservation sub title","cinnamon-restaurant"),
						"id"			=> "reservation_sub_title",
						"std"			=> "Online",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__("Reservation title","cinnamon-restaurant"),
						"desc"			=> esc_html__("Reservation title","cinnamon-restaurant"),
						"id"			=> "reservation_title",
						"std"			=> "Reservation",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__("Reservation button text","cinnamon-restaurant"),
						"desc"			=> esc_html__("Reservation button text","cinnamon-restaurant"),
						"id"			=> "reservation_button_title",
						"std"			=> "Reservation",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__("Reservation Slide text", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Add text for reservation slide text", "cinnamon-restaurant" ),
						"id"			=> "reservation_description",
						"std"			=> 'Integer congue malesuada eros congue varius. Sed malesuada dolor eget velit pretium. Etiam porttitor finibus. Nam suscipit vel ligula at dharetra',
						"type"			=> "textarea");

	$options[] = array( "name"			=> esc_html__("Reservation shortcode","cinnamon-restaurant"),
						"desc"			=> esc_html__("Reservation shortcode. For opentable form add ", "cinnamon-restaurant" ) . "<code>[opentableform]</code>",
						"id"			=> "reservation_shortcode",
						"std"			=> "",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array(	"name"			=> esc_html__("Prefered reservation page instead of shortcode", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Prefered reservation page.", "cinnamon-restaurant" ),
						"id"			=> "reservation_page",
						"std"			=> '',
						"type"			=> "selectyper",
						"class"			=> "small", //mini, tiny, small
						"options"			=> $options_pages);

	$options[] = array( "name"			=> esc_html__("Reservation url","cinnamon-restaurant"),
						"desc"			=> esc_html__("Reservation url which over-ride shortcode and page", "cinnamon-restaurant" ),
						"id"			=> "reservation_url",
						"std"			=> "",
						"class"			=> "tiny",
						"type"			=> "text");

$options[] = array( "name"			=> esc_html__("OpenTable", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-cutlery',
					"type"			=> "heading");

	$options[] = array( "name"			=> esc_html__("OpenTable ID","cinnamon-restaurant"),
						"desc"			=> esc_html__("OpenTable ID","cinnamon-restaurant"),
						"id"			=> "opentable_id",
						"std"			=> "",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__("Person singular referer","cinnamon-restaurant"),
						"desc"			=> esc_html__("Person singular referer. eg: Person","cinnamon-restaurant"),
						"id"			=> "person_singular",
						"std"			=> "Person",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__("Person plural referer","cinnamon-restaurant"),
						"desc"			=> esc_html__("Person plural referer. eg: People","cinnamon-restaurant"),
						"id"			=> "person_plural",
						"std"			=> "People",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__("Button Text","cinnamon-restaurant"),
						"desc"			=> esc_html__("Button Text","cinnamon-restaurant"),
						"id"			=> "opentable_button_text",
						"std"			=> "Find a Table",
						"class"			=> "tiny",
						"type"			=> "text");

$options[] = array( "name" => esc_html__("404", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-question',
					"type" => "heading");

		$options[] = array( "name" => esc_html__( "404 Background image ( for page not found. Optional )", 'cinnamon-restaurant' ),
							"desc" => esc_html__( "Upload background image", 'cinnamon-restaurant' ),
							"id" => "general_404_image",
							"type" => "upload");
						
		$options[] = array( "name"			=> esc_html__("404 Page title", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("404 Page not Found!", "cinnamon-restaurant" ),
						"id"			=> "404_title",
						"std"			=> "",
						"class"			=> "small",
						"type"			=> "text");

$options[] = array( "name"			=> esc_html__("Events", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-paper-plane-o',
					"type"			=> "heading");

	$options[] = array( "name" => esc_html__( "Events Time format", 'cinnamon-restaurant' ),
						"desc" => esc_html__( "Events Time format", 'cinnamon-restaurant' ),
						"id" => "events_time_format",
						"std" => "true",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => array(
							'conventional' => "AM/PM",
							'24hr' => "24 Hrs")
						);

	$options[] = array( "name"			=> esc_html__("Event gallery title","cinnamon-restaurant"),
						"desc"			=> esc_html__("Event gallery title","cinnamon-restaurant"),
						"id"			=> "event_gallery_title",
						"std"			=> "Events",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array(	"name"			=> esc_html__("Prefered events archive page", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Prefered events archive page.", "cinnamon-restaurant" ),
						"id"			=> "events_archive_page",
						"std"			=> '',
						"type"			=> "selectyper",
						"class"			=> "small", //mini, tiny, small
						"options"			=> $options_pages);

	$options[] = array( "name"			=> esc_html__("Number of thumbnail columns for Events archive listing", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Affects events archives. eg. Browsing Events category links.", "cinnamon-restaurant" ),
						"id"			=> "event_achivelisting",
						"min"			=> "1",
						"max"			=> "4",
						"step"			=> "0",
						"unit"			=> 'columns',
						"std"			=> "3",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__("Postponed message","cinnamon-restaurant"),
						"desc"			=> esc_html__("Message for postponed events","cinnamon-restaurant"),
						"id"			=> "events_postponed_msg",
						"std"			=> "Event Postponed",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__("Cancelled message","cinnamon-restaurant"),
						"desc"			=> esc_html__("Message for cancelled events","cinnamon-restaurant"),
						"id"			=> "events_cancelled_msg",
						"std"			=> "Event Cancelled",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__("Event Time Text","cinnamon-restaurant"),
						"desc"			=> esc_html__("Text to use for Event time","cinnamon-restaurant"),
						"id"			=> "events_time_text",
						"std"			=> "When",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__("Event Location Text","cinnamon-restaurant"),
						"desc"			=> esc_html__("Text to use for Event location","cinnamon-restaurant"),
						"id"			=> "events_location_text",
						"std"			=> "Where",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__("Event Price Text","cinnamon-restaurant"),
						"desc"			=> esc_html__("Text to use for Event price","cinnamon-restaurant"),
						"id"			=> "events_price_text",
						"std"			=> "Cost",
						"class"			=> "tiny",
						"type"			=> "text");

$options[] = array( "name"			=> esc_html__("Blog", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-th',
					"type"			=> "heading");
					
	$options[] = array( "name"			=> esc_html__("Display Fullpost Archives", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Display fullpost archives", "cinnamon-restaurant" ),
						"id"			=> "postformat_fullcontent",
						"std"			=> "0",
						"type"			=> "checkbox");

	$options[] = array( "name"			=> esc_html__("Display Author Bio", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Display Author Bio", "cinnamon-restaurant" ),
						"id"			=> "author_bio",
						"std"			=> "0",
						"type"			=> "checkbox");
						
	$options[] = array( "name"			=> esc_html__("Hide allowed HTML tags info", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Hide allowed HTML tags info after comments box", "cinnamon-restaurant" ),
						"id"			=> "blog_allowedtags",
						"std"			=> "0",
						"type"			=> "checkbox");

	$options[] = array( "name"			=> esc_html__("Read more text", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Enter text for Read more", "cinnamon-restaurant" ),
						"id"			=> "read_more",
						"std"			=> "Continue reading",
						"class"			=> "small",
						"type"			=> "text");

$options[] = array( "name"			=> esc_html__("Page Title", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-header',
					"type"			=> "heading");

	$options[] = array( "name"			=> esc_html__("Hide Page title", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Hide Page title from all pages", "cinnamon-restaurant" ),
						"id"			=> "hide_pagetitle",
						"std"			=> "0",
						"type"			=> "checkbox");

	$options[] = array( "name"			=> esc_html__("Page title size", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Page title size in pixels. eg. 42", "cinnamon-restaurant" ),
						"id"			=> "pagetitle_size",
						"std"			=> "",
						"class"			=> "small",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__("Page title letter spacing", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Page title letter spacing in pixels. eg. 6", "cinnamon-restaurant" ),
						"id"			=> "pagetitle_letterspacing",
						"std"			=> "",
						"class"			=> "small",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__("Page title weight", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Page title weight. eg. 100, 200, 300, 400, 500, 600, 700, 800, 900", "cinnamon-restaurant" ),
						"id"			=> "pagetitle_weight",
						"std"			=> "",
						"class"			=> "small",
						"type"			=> "text");

$options[] = array( "name"			=> esc_html__("Menu Text", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-text-height',
					"type"			=> "heading");

	$options[] = array( "name"			=> esc_html__("Horizontal menu text size", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Horizontal menu text size. eg. 12", "cinnamon-restaurant" ),
						"id"			=> "hmenu_text_size",
						"std"			=> "",
						"class"			=> "small",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__("Horizontal menu text letter spacing", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Horizontal menu text letter spacing in pixels. eg. 6", "cinnamon-restaurant" ),
						"id"			=> "hmenu_text_letterspacing",
						"std"			=> "",
						"class"			=> "small",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__("Horizontal menu text weight", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Horizontal menu text weight. eg. 100, 200, 300, 400, 500, 600, 700, 800, 900", "cinnamon-restaurant" ),
						"id"			=> "hmenu_text_weight",
						"std"			=> "",
						"class"			=> "small",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__("Menu item gap", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Menu item gap. eg. 20", "cinnamon-restaurant" ),
						"id"			=> "hmenu_item_gap",
						"std"			=> "",
						"class"			=> "small",
						"type"			=> "text");

$options[] = array( "name"			=> esc_html__("Content Text", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-text-height',
					"type"			=> "heading");

		$options[] = array( "name"			=> esc_html__("Content Font Size", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Contents font size in pixels.", "cinnamon-restaurant" ),
						"id"			=> "pagecontent_fontsize",
						"std"			=> "",
						"class"			=> "small",
						"type"			=> "text");

		$options[] = array( "name"			=> esc_html__("Content line height", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Contents line height in pixels.", "cinnamon-restaurant" ),
						"id"			=> "pagecontent_lineheight",
						"std"			=> "",
						"class"			=> "small",
						"type"			=> "text");

		$options[] = array( "name"			=> esc_html__("Content letter spacing", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Content letter spacing in pixels.", "cinnamon-restaurant" ),
						"id"			=> "pagecontent_letterspacing",
						"std"			=> "",
						"class"			=> "small",
						"type"			=> "text");

		$options[] = array( "name"			=> esc_html__("Content font weight", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Content font weight.(100,200,300,400,500,600,700,800,900)", "cinnamon-restaurant" ),
						"id"			=> "pagecontent_fontweight",
						"std"			=> "",
						"class"			=> "small",
						"type"			=> "text");

$options[] = array( "name"			=> esc_html__("Lightbox", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-star',
					"type"			=> "heading");

	$options[] = array( "name"			=> esc_html__("Disable Lightbox fullscreen button", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Disable Lightbox fullscreen", "cinnamon-restaurant" ),
						"id"			=> "disable_lightbox_fullscreen",
						"std"			=> "0",
						"type"			=> "checkbox");

	$options[] = array( "name"			=> esc_html__("Disable Lightbox actual size toggle", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Disable Lightbox actual size toggle", "cinnamon-restaurant" ),
						"id"			=> "disable_lightbox_sizetoggle",
						"std"			=> "0",
						"type"			=> "checkbox");

	$options[] = array( "name"			=> esc_html__("Disable Lightbox zoom controls", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Disable Lightbox zoom controls", "cinnamon-restaurant" ),
						"id"			=> "disable_lightbox_zoomcontrols",
						"std"			=> "0",
						"type"			=> "checkbox");

	$options[] = array( "name"			=> esc_html__("Disable Lightbox autoplay button", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Disable Lightbox autoplay button", "cinnamon-restaurant" ),
						"id"			=> "disable_lightbox_autoplay",
						"std"			=> "0",
						"type"			=> "checkbox");

	$options[] = array( "name"			=> esc_html__("Disable Lightbox share button", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Disable Lightbox share button", "cinnamon-restaurant" ),
						"id"			=> "disable_lightbox_share",
						"std"			=> "0",
						"type"			=> "checkbox");

	$options[] = array( "name"			=> esc_html__("Disable Lightbox count button", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Disable Lightbox count button", "cinnamon-restaurant" ),
						"id"			=> "disable_lightbox_count",
						"std"			=> "0",
						"type"			=> "checkbox");

	$options[] = array( "name" => esc_html__( "Lightbox Transition", 'cinnamon-restaurant' ),
						"desc" => esc_html__( "Lightbox Transition", 'cinnamon-restaurant' ),
						"id" => "lightbox_transition",
						"std" => "true",
						"type" => "select",
						"class" => "mini",
						"options" 	=> array(
							'lg-slide'                       => 'Slide',
							'lg-fade'                        => 'Fade',
							'lg-zoom-in'                     => 'Zoom in',
							'lg-zoom-in-big'                 => 'Zoom in Big', 
							'lg-zoom-out'                    => 'Zoom Out',
							'lg-zoom-out-big'                => 'Zoom Out big', 
							'lg-zoom-out-in'                 => 'Zoom Out in',
							'lg-zoom-in-out'                 => 'Zoom in Out',
							'lg-soft-zoom'                   => 'Soft Zoom', 
							'lg-scale-up'                    => 'Scale Up', 
							'lg-slide-circular'              => 'Slide Circular', 
							'lg-slide-circular-vertical'     => 'Slide Circular vertical', 
							'lg-slide-vertical'              => 'Slide Vertical', 
							'lg-slide-vertical-growth'       => 'Slide Vertical growth', 
							'lg-slide-skew-only'             => 'Slide Skew only', 
							'lg-slide-skew-only-rev'         => 'Slide Skew only reverse',
							'lg-slide-skew-only-y'           => 'Slide Skew only y', 
							'lg-slide-skew-only-y-rev'       => 'Slide Skew only y reverse',
							'lg-slide-skew'                  => 'Slide Skew', 
							'lg-slide-skew-rev'              => 'Slide Skew reverse',
							'lg-slide-skew-cross'            => 'Slide Skew cross', 
							'lg-slide-skew-cross-rev'        => 'Slide Skew cross reverse',
							'lg-slide-skew-ver'              => 'Slide Skew vertically', 
							'lg-slide-skew-ver-rev'          => 'Slide Skew vertically reverse',
							'lg-slide-skew-ver-cross'        => 'Slide Skew vertically cross', 
							'lg-slide-skew-ver-cross-rev'    => 'Slide Skew vertically cross reverse',
							'lg-lollipop'                    => 'Lollipop', 
							'lg-lollipop-rev'                => 'Lollipop reverse',
							'lg-rotate'                      => 'Rotate', 
							'lg-rotate-rev'                  => 'Rotate reverse',
							'lg-tube'                        => 'Tube',
							)
						);

	$options[] = array( "name"			=> esc_html__("Disable Lightbox title", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Disable Lightbox title", "cinnamon-restaurant" ),
						"id"			=> "disable_lightbox_title",
						"std"			=> "0",
						"type"			=> "checkbox");

	$options[] = array( "name"			=> esc_html__( "Lightbox background color", "cinnamon-restaurant" ),
						"desc"			=> esc_html__( "Lightbox background color", "cinnamon-restaurant" ),
						"id"			=> "lightbox_bgcolor",
						"std"			=> "",
						"type"			=> "color");

	$options[] = array( "name"			=> esc_html__( "Lightbox element background color", "cinnamon-restaurant" ),
						"desc"			=> esc_html__( "Lightbox element background color", "cinnamon-restaurant" ),
						"id"			=> "lightbox_elementbgcolor",
						"std"			=> "",
						"type"			=> "color");

	$options[] = array( "name"			=> esc_html__( "Lightbox element color", "cinnamon-restaurant" ),
						"desc"			=> esc_html__( "Lightbox element color", "cinnamon-restaurant" ),
						"id"			=> "lightbox_elementcolor",
						"std"			=> "",
						"type"			=> "color");

$options[] = array( "name"			=> esc_html__("Sidebars", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-newspaper-o',
					"type"			=> "heading");

	$max_sidebars = cinnamon_restaurant_get_max_sidebars();
	for ($sidebar_count=1; $sidebar_count <= $max_sidebars; $sidebar_count++ ) {

	$options[] = array( "name"			=> esc_html__("Sidebar ", "cinnamon-restaurant" ) . $sidebar_count,
							"type"			=> "info");
						
		$options[] = array( "name"			=> esc_html__("Sidebar Name", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Activate sidebars by naming them.", "cinnamon-restaurant" ),
						"id"			=> "mthemesidebar-".$sidebar_count,
						"std"			=> "",
						"class"			=> "small",
						"type"			=> "text");

		$options[] = array( "name"			=> esc_html__("Sidebar Description", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("A small description to display inside the widget to easily identify it. Widget description is only shown in admin mode inside the widget.", "cinnamon-restaurant" ),
						"id"			=> "theme_sidebardesc".$sidebar_count,
						"std"			=> "",
						"class"			=> "small",
						"type"			=> "text");
	}

$options[] = array( "name"			=> esc_html__("Accent Color", "cinnamon-restaurant" ),
				"icon"			=> 'fa fa-paint-brush',
				"type"			=> "heading",
				"subheading"			=> 'header_section_order');

		$options[] = array( "name"			=> esc_html__( "Accent color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Accent color", "cinnamon-restaurant" ),
							"id"			=> "theme_accent_color",
							"std"			=> "",
							"type"			=> "color");

$options[] = array( "name"			=> esc_html__("Menu Color", "cinnamon-restaurant" ),
				"icon"			=> 'fa fa-paint-brush',
				"type"			=> "heading",
				"subheading"			=> 'header_section_order');

		$options[] = array( "name"			=> esc_html__( "Menu color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Menu color", "cinnamon-restaurant" ),
							"id"			=> "menubar_backgroundcolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name" => esc_html__( "Menu bar opacity ( default 80 )", 'cinnamon-restaurant' ),
							"desc" => esc_html__( "Note: Only applies if a color is set as menu color", 'cinnamon-restaurant' ),
							"id" => "menubar_backgroundopacity",
							"min" => "0",
							"max" => "100",
							"step" => "0",
							"unit" => '%',
							"std" => "80",
							"type" => "text");

		$options[] = array( "name" => esc_html__( "Menu bar opacity for stickymenu ( default 80 )", 'cinnamon-restaurant' ),
							"desc" => esc_html__( "Note: Only applies if a color is set as menu color", 'cinnamon-restaurant' ),
							"id" => "stickymenubar_backgroundopacity",
							"min" => "0",
							"max" => "100",
							"step" => "0",
							"unit" => '%',
							"std" => "80",
							"type" => "text");

		$options[] = array( "name"			=> esc_html__( "Menu link color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Menu link color", "cinnamon-restaurant" ),
							"id"			=> "menu_linkcolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Menu link hover color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Menu link hover color", "cinnamon-restaurant" ),
							"id"			=> "menu_linkhovercolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Current menu link color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Current menu link color", "cinnamon-restaurant" ),
							"id"			=> "currentmenu_linkcolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Submenu background color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Submenu  background color", "cinnamon-restaurant" ),
							"id"			=> "menusubcat_bgcolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Submenu link color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Submenu link color", "cinnamon-restaurant" ),
							"id"			=> "menusubcat_linkcolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Submenu link hover color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Submenu link hover color", "cinnamon-restaurant" ),
							"id"			=> "menusubcat_linkhovercolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Submenu link underline color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Submenu link underline color", "cinnamon-restaurant" ),
							"id"			=> "menusubcat_linkunderlinecolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Current Submenu link color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Current Submenu link color", "cinnamon-restaurant" ),
							"id"			=> "currentmenusubcat_linkcolor",
							"std"			=> "",
							"type"			=> "color");

$options[] = array( "name"			=> esc_html__("Responsive Menu", "cinnamon-restaurant" ),
				"icon"			=> 'fa fa-paint-brush',
				"type"			=> "heading",
				"subheading"			=> 'header_section_order');

		$options[] = array( "name"			=> esc_html__( "Responsive Menu bar color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Responsive Menu bar color", "cinnamon-restaurant" ),
							"id"			=> "mobilemenubar_bgcolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Responsive Menu toggle icon color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Responsive Menu toggle icon color", "cinnamon-restaurant" ),
							"id"			=> "mobilemenubar_togglecolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Responsive Menu background color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Responsive Menu background color", "cinnamon-restaurant" ),
							"id"			=> "mobilemenu_bgcolor",
							"std"			=> "",
							"type"			=> "color");

	$options[] = array( "name"			=> esc_html__( "Use an image as Responsive menu background", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Use an image as Responsive menu background", 'cinnamon-restaurant' ),
						"id"			=> "mobilemenu_bgimage",
						"type"			=> "upload");

		$options[] = array( "name"			=> esc_html__( "Responsive Menu text and icon color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Responsive Menu text and icon color", "cinnamon-restaurant" ),
							"id"			=> "mobilemenu_texticons",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Responsive Menu hover color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Responsive Menu hover color", "cinnamon-restaurant" ),
							"id"			=> "mobilemenu_hover",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Responsive Menu active color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Responsive Menu active color", "cinnamon-restaurant" ),
							"id"			=> "mobilemenu_active",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Responsive Menu line colors", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Responsive Menu line colors", "cinnamon-restaurant" ),
							"id"			=> "mobilemenu_linecolors",
							"std"			=> "",
							"type"			=> "color");

$options[] = array( "name"			=> esc_html__("Vertical Menu Color", "cinnamon-restaurant" ),
				"icon"			=> 'fa fa-paint-brush',
				"type"			=> "heading",
				"subheading"			=> 'header_section_order');

	$options[] = array( "name"			=> esc_html__( "Use an image as Vertical menu background", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Use an image as Vertical menu background", 'cinnamon-restaurant' ),
						"id"			=> "verticalmenu_bgimage",
						"type"			=> "upload");

		$options[] = array( "name"			=> esc_html__( "Menu background color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Menu background color", "cinnamon-restaurant" ),
							"id"			=> "vmenubar_bgcolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Menu link color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Menu link color", "cinnamon-restaurant" ),
							"id"			=> "vmenubar_linkcolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Menu horizontal lines", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Menu horizontal lines", "cinnamon-restaurant" ),
							"id"			=> "vmenubar_hlinecolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Menu link hover color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Menu link hover color", "cinnamon-restaurant" ),
							"id"			=> "vmenubar_linkhovercolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Active Menu link color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Active Menu link color", "cinnamon-restaurant" ),
							"id"			=> "vmenubar_linkactivecolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Social icon color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Social icon color", "cinnamon-restaurant" ),
							"id"			=> "vmenubar_socialcolor",
							"std"			=> "",
							"type"			=> "color");

$options[] = array( "name"				=> esc_html__("Page Color", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-paint-brush',
					"type"				=> "heading",
					"subheading"		=> 'header_section_order');

		$options[] = array( "name"			=> esc_html__( "Page background color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Page background color", "cinnamon-restaurant" ),
							"id"			=> "page_bgcolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Page contents color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Page contents color", "cinnamon-restaurant" ),
							"id"			=> "page_contentscolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Page contents heading color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Page contents heading color", "cinnamon-restaurant" ),
							"id"			=> "page_contentsheading",
							"std"			=> "",
							"type"			=> "color");

$options[] = array( "name"				=> esc_html__("Sidebar Color", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-paint-brush',
					"type"				=> "heading",
					"subheading"		=> 'header_section_order');

		$options[] = array( "name"			=> esc_html__( "Sidebar heading color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Sidebar heading color", "cinnamon-restaurant" ),
							"id"			=> "sidebar_headingcolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Sidebar link color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Sidebar link color", "cinnamon-restaurant" ),
							"id"			=> "sidebar_linkcolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Sidebar text color", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Sidebar text color", "cinnamon-restaurant" ),
							"id"			=> "sidebar_textcolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> esc_html__( "Sidebar heading backgrounds", "cinnamon-restaurant" ),
							"desc"			=> esc_html__( "Sidebar heading backgrounds", "cinnamon-restaurant" ),
							"id"			=> "sidebar_bgcolor",
							"std"			=> "",
							"type"			=> "color");

	$options[] = array( "name" => "Footer Color",
					"icon"			=> 'fa fa-paint-brush',
					"type" => "heading",
					"subheading" => 'header_section_order');

		$options[] = array( "name" => "Footer background",
							"desc" => "Footer background",
							"id" => "footer_bgcolor",
							"std" => "",
							"type" => "color");

	$options[] = array( "name" => "Footer text color",
						"desc" => "Footer text color",
						"id" => "footer_text",
						"std" => "",
						"type" => "color");

	$options[] = array( "name" => "Footer text link color",
						"desc" => "Footer text link color",
						"id" => "footer_link",
						"std" => "",
						"type" => "color");

	$options[] = array( "name" => "Footer text link hover color",
						"desc" => "Footer text link hover color",
						"id" => "footer_linkhover",
						"std" => "",
						"type" => "color");

	$options[] = array( "name" => "Footer icon color",
						"desc" => "Footer icon color",
						"id" => "footer_iconcolor",
						"std" => "",
						"type" => "color");

						
$options[] = array( "name"			=> esc_html__("Fonts", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-font',
					"type"			=> "heading");
					
$options[] = array( "name"			=> esc_html__("Enable Google Web Fonts", "cinnamon-restaurant" ),
					"desc"			=> esc_html__("Enable Google Web fonts", "cinnamon-restaurant" ),
					"id"			=> "default_googlewebfonts",
					"std"			=> "0",
					"type"			=> "checkbox");

	$options[] = array(	"name"			=> esc_html__("Section title","cinnamon-restaurant"),
						"desc"			=> esc_html__("Select font for section title","cinnamon-restaurant"),
						"id"			=> "section_font_title",
						"std"			=> 'Default Font',
						"type"			=> "selectgooglefont",
						"class"			=> "small", //mini, tiny, small
						"options"			=> $options_fonts);	

	$options[] = array(	"name"			=> esc_html__("Section subtitle","cinnamon-restaurant"),
						"desc"			=> esc_html__("Select font for section subtitle","cinnamon-restaurant"),
						"id"			=> "section_font_subtitle",
						"std"			=> 'Default Font',
						"type"			=> "selectgooglefont",
						"class"			=> "small", //mini, tiny, small
						"options"			=> $options_fonts);	
						
	$options[] = array(	"name"			=> esc_html__("Menu Font", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Select menu font", "cinnamon-restaurant" ),
						"id"			=> "menu_font",
						"std"			=> '',
						"type"			=> "selectgooglefont",
						"class"			=> "small", //mini, tiny, small
						"options"			=> $options_fonts);
						
	$options[] = array(	"name"			=> esc_html__("Heading Font (applies to all headings)", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Select heading font", "cinnamon-restaurant" ),
						"id"			=> "heading_font",
						"std"			=> '',
						"type"			=> "selectgooglefont",
						"class"			=> "small", //mini, tiny, small
						"options"			=> $options_fonts);	
						
	$options[] = array(	"name"			=> esc_html__("Contents", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Select font for headings inside posts and pages", "cinnamon-restaurant" ),
						"id"			=> "page_contents",
						"std"			=> '',
						"type"			=> "selectgooglefont",
						"class"			=> "small", //mini, tiny, small
						"options"			=> $options_fonts);

$options[] = array( "name"			=> esc_html__("Custom Font", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-font',
					"type"			=> "heading",
					"subheading"			=> 'default_googlewebfonts');

	$options[] = array( "name"			=> esc_html__("Font Embed Code", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("example", "cinnamon-restaurant" ) . "<br/><code>&lt;link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'&gt;</code>",
						"id"			=> "custom_font_embed",
						"std"			=> '',
						"type"			=> "textarea");

	$options[] = array( "name"			=> esc_html__("CSS Codes for Custom Font", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("example", "cinnamon-restaurant" ) . "<br/><code>.entry-title h1 { font-family: 'Lobster', cursive; }</code>",
						"id"			=> "custom_font_css",
						"std"			=> '',
						"type"			=> "textarea");

$options[] = array( "name"			=> esc_html__("WPML", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-globe',
					"type"			=> "heading");

	$options[] = array( "name"			=> esc_html__( "Enable Built-in WPML language selector", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Enable Built-in WPML language selector", 'cinnamon-restaurant' ),
						"id"			=> "wpml_lang_selector_enable",
						"std"			=> "0",
						"type"			=> "checkbox");

$options[] = array( "name"			=> esc_html__("WooCommerce", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-shopping-cart',
					"type"			=> "heading");

		$options[] = array( "name"			=> esc_html__("WooCommerce Shop default title", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Shop title for WooCommerce shop. ( default 'Shop' ).", "cinnamon-restaurant" ),
						"id"			=> "mtheme_woocommerce_shoptitle",
						"std"			=> "Shop",
						"class"			=> "small",
						"type"			=> "text");

	$options[] = array( "name"			=> esc_html__( "Fullwidth Shop Archive", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Display fullwidth shop archive", 'cinnamon-restaurant' ),
						"id"			=> "mtheme_wooarchive_fullwidth",
						"std"			=> "0",
						"type"			=> "checkbox");

	$options[] = array( "name"			=> esc_html__( "Disable Menu Cart icon", 'cinnamon-restaurant' ),
						"desc"			=> esc_html__( "Disable Menu Cart icon", 'cinnamon-restaurant' ),
						"id"			=> "mtheme_woocart_menu",
						"std"			=> "0",
						"type"			=> "checkbox");
						
$options[] = array( "name"			=> esc_html__("Footer", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-arrows-h',
					"type"			=> "heading");

	$options[] = array( "name"			=> "Widget Footer Enabled",
						"desc"			=> "Display Footer Widgets",
						"id"			=> "footerwidget_status",
						"std"			=> "1",
						"type"			=> "checkbox");
					
	$options[] = array( "name"			=> esc_html__("Copyright text", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Enter your copyright and other texts to display in footer", "cinnamon-restaurant" ),
						"id"			=> "footer_copyright",
						"std"			=> "Copyright &copy; 2018",
						"type"			=> "textarea");

		$options[] = array( "name"			=> esc_html__("Copyright font size", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Copyright font size in pixels.", "cinnamon-restaurant" ),
						"id"			=> "copyright_fontsize",
						"std"			=> "",
						"class"			=> "small",
						"type"			=> "text");

		$options[] = array( "name"			=> esc_html__("Copyright line height", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Copyright line height in pixels.", "cinnamon-restaurant" ),
						"id"			=> "copyright_lineheight",
						"std"			=> "",
						"class"			=> "small",
						"type"			=> "text");

$options[] = array( "name"			=> esc_html__("Export", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-upload',
					"type"			=> "heading");

	$options[] = array( "name"			=> esc_html__("Export Options ( Copy this ) Read-Only.", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("Select All, copy and store your theme options backup. You can use these value to import theme options settings.", "cinnamon-restaurant" ),
						"id"			=> "exportpack",
						"std"			=> '',
						"class"			=> "big",
						"type"			=> "exporttextarea");

$options[] = array( "name"			=> esc_html__("Import Options", "cinnamon-restaurant" ),
					"icon"			=> 'fa fa-download',
					"type"			=> "heading",
					"subheading"		=> 'exportpack');

	$options[] = array( "name"			=> esc_html__("Import Options ( Paste and Save )", "cinnamon-restaurant" ),
						"desc"			=> esc_html__("CAUTION: Copy and Paste the Export Options settings into the window and Save to apply theme options settings.", "cinnamon-restaurant" ),
						"id"			=> "importpack",
						"std"			=> '',
						"class"			=> "big",
						"type"			=> "importtextarea");

	return $options;
}
?>