<?php
function cinnamon_restaurant_woocommerce_metadata() {
$mtheme_sidebar_options = cinnamon_restaurant_generate_sidebarlist("portfolio");

$mtheme_imagepath =  get_template_directory_uri() . '/framework/options/images/metaboxes/';

$mtheme_woocommerce_box = array(
	'id' => 'woocommercemeta-box',
	'title' => esc_html__('Woocommerce Metabox','cinnamon-restaurant'),
	'page' => 'page',
	'context' => 'normal',
	'priority' => 'high',
	'fields' => array(
			array(
				'name' => esc_html__('Page Settings','cinnamon-restaurant'),
				'id' => 'pagemeta_background_section_id',
				'type' => 'break',
				'sectiontitle' => esc_html__('Page Background','cinnamon-restaurant'),
				'std' => ''
				),
			array(
				'name' => esc_html__('Page Settings','cinnamon-restaurant'),
				'id' => 'pagemeta_sep-page_backgrounds',
				'type' => 'seperator',
				),
			array(
				'name' => esc_html__('Header Type','cinnamon-restaurant'),
				'id' => 'pagemeta_header_type',
				'type' => 'select',
				'std' => 'enable',
				'desc' => esc_html__('Header Type for Horizontal menu','cinnamon-restaurant'),
				'options' => array(
					'default' => esc_html__('Default','cinnamon-restaurant'),
					'transparent' => esc_html__('Transparent','cinnamon-restaurant'),
					'transparent-invert' => esc_html__('Transparent Inverse','cinnamon-restaurant')
					)
				),
			array(
				'name' => esc_html__('Switch Menu','cinnamon-restaurant'),
				'id' => 'pagemeta_menu_choice',
				'type' => 'select',
				'desc' => esc_html__('Select a different menu for this page','cinnamon-restaurant'),
				'options' => cinnamon_restaurant_generate_menulist()
				),
			array(
				'name' => esc_html__('Page Title','cinnamon-restaurant'),
				'id' => 'pagemeta_page_title',
				'type' => 'select',
				'desc' => esc_html__('Page Title','cinnamon-restaurant'),
				'std' => 'default',
				'options' => array(
					'default' => esc_html__('Default','cinnamon-restaurant'),
					'show' => esc_html__('Show','cinnamon-restaurant'),
					'hide' => esc_html__('Hide','cinnamon-restaurant')
					)
				)
		)
);
return $mtheme_woocommerce_box;
}
add_action("admin_init", "cinnamon_restaurant_woocommerceitemmetabox_init");
function cinnamon_restaurant_woocommerceitemmetabox_init(){
	add_meta_box("mtheme_woocommerceInfo-meta", esc_html__("WooCommerce Options","cinnamon-restaurant"), "cinnamon_restaurant_woocommerceitem_metaoptions", "product", "normal", "low");
}
/*
* Meta options for Portfolio post type
*/
function cinnamon_restaurant_woocommerceitem_metaoptions(){
	$mtheme_woocommerce_box = cinnamon_restaurant_woocommerce_metadata();
	cinnamon_restaurant_generate_metaboxes($mtheme_woocommerce_box,get_the_id());
}
?>